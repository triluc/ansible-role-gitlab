#!/usr/bin/python

# Copyright (c) 2018-present eyeo GmbH
# GNU General Public License v3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview', 'stableinterface'],
    'supported_by': 'community',
}

DOCUMENTATION = """
---
module: gitlab_runner

short_description: Manage gitlab-runner instances

version_added: "2.4"

description: >
    This module (un)registers gitlab-runner instances with a given set of
    parameters. When desired, it also marks runners to run jobs only on
    protected Branches.

options:
    flags:
        description: >
            A list of flags to be passed to the registration process. see
            $ gitlab-runner register --help
            for a list of valid flags. Flags are suffixed with '=true'
            automatically, in case they don't end with '=true' or '=false'.
        type: list
        default: []

    parameters:
        description: >
            Collection of parameters to pass to the (un)register command
        type: dict
        required: true
        suboptions:
            executor:
                description: >
                    Executor type of the managed runner
                type: str
                default: shell

            name:
                description: >
                    The name of the runner to manage
                type: str
                required: true

            registration-token:
                description: >
                    Token to register the runner with
                type: str
                required: true

            url:
                description: >
                    Host to manage the runner on
                type: str
                default: https://gitlab.com
            extra:
                description: >
                    Additional parameters. see
                    $ gitlab-runner register --help
                    for a list of valid key/value pairs.
                type: dict
                default: {}

    personal_access_token:
        description: >
            The administrators / maintainers personal_access_token
            generated at I(parameters.url)
            Required when I(protected=true) or I(protected=false)
        type: str
        default: null

    protected:
        description: >
            Whether the runner should only run jobs for protected branches
            Specifying this option requires I(personal_access_token) to be
            defined
        type: bool
        default: null

    state:
        description: >
            Assert the state of the runner.
        default: present
        choices:
            - absent
            - present
        type: str

author:
    - eyeo GmbH
"""

EXAMPLES = """
- name: register a not protected runner "example-runner"
  gitlab_runner:
    parameters:
      name: example-runner
      registration-token: aaabbbcccddd
    flags:
      - run-untagged

- name: register a runner with the tags "test" and "example"
  gitlab_runner:
    parameters:
      name: example-runner
      registration-token: aaabbbcccddd
      extra:
        tag-list: "test,example"

- name: register a protected runner "example-protected" @ https://example.com
  gitlab_runner:
    parameters:
      name: example-protected
      registration-token: bbbaaadddccc
      url: https://example.com
    flags:
      - run-untagged
    protected: true
    personal_access_token: asdogasdXXXXasdasd

"""

RETURN = """
name:
    description: The name of the managed runner
    type: str
    returned: on success

url:
    description: The location where the runner is managed
    type: str
    returned: on success

executor:
    description: Exectuor type of the managed runner
    type: str
    returned: on success

state:
    description: Resulting state of the runner
    type: str
    returned: on success
"""

try:
    from urllib.parse import urlencode
except ImportError:
    from urllib import urlencode

import json
import re
import yaml

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.urls import open_url

RUNNER_REMOVED = """
Manual action required: Runner with token {} was removed via the UI @{} and can
 no longer be properly managed by "gitlab-runner (un)register".
"""

UNKNOWN_STATE = 'The state of "{}" could not be determined.'


class GitLabRunnerModule(AnsibleModule):
    """Manage gitlab-runner instances as an Ansible module.

    This class provides a module for Ansible in order to manage exactly one
    gitlab-runner instance. Given the necessary parameters, it is capable of
    registering, unregistering, protecting and unprotecting an instance.

    See `$ ansible-doc -t module manage-gitlab-runner` for more information
    about possible parameters.

    While being capable of parsing an abitrary amount of paramters in
    `paramters.extra`, this Module only changes the state (and marks the
    instance as changed) when either `state` or `protected` are changed.
    Additionally, any parameter passed to `parameters.extra` will affect the
    instance only during creation (i.e. when the instance is freshly registered
    on the host).

    Note: This module requires `gitlab-runner` installed on the host.
    """

    _required_together = [
        ['protected', 'personal_access_token'],
    ]
    _cmd = 'gitlab-runner'
    _runners_url = 'https://gitlab.com/api/v4/runners'

    def _runner_exists(self):
        rc, out, err = self.run_command(
            [self._cmd, 'verify', '--name', self.gitlab_parameters['name']],
        )

        # Strip escape sequences from output and error
        ansi_escape = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')
        err = ansi_escape.sub('', err)
        out = ansi_escape.sub('', out)

        if rc == 0:
            return True

        if 'no runner matches the filtering parameters' in err.lower():
            # Not registered yet
            return False

        if 'verifying runner... is removed' in err.lower():
            # Runner removed via UI -> FATAL
            runner_token = re.search(r'runner=(\S+)', err).group(1)

            self.fail_json(
                msg=RUNNER_REMOVED.format(runner_token,
                                          self.gitlab_parameters['url']),
            )

        # Unknown state
        self.fail_json(
            msg=UNKNOWN_STATE.format(self.gitlab_parameters['name']),
        )

    def _gitlab_api_request(self, url, method='GET', data=None):
        result = open_url(
            url,
            method=method,
            data=urlencode(data) if data else None,
            headers={'Private-Token': self.params['personal_access_token']},
        )

        try:
            paginators = re.findall(
                r'<(.+?)>; rel=\"(\S+)\"',
                result.headers.dict['link'],
            )
            nexturl = {x[1]: x[0] for x in paginators}['next']
        except KeyError:
            nexturl = None

        return json.load(result), nexturl

    @property
    def _runner_id(self):
        if self.__runner_id is None:
            name = self.gitlab_parameters['name']
            nexturl = self._runners_url
            while nexturl:
                data, nexturl = self._gitlab_api_request(nexturl)
                try:
                    data = [x for x in data
                            if x['description'] == name][0]
                    break
                except IndexError:
                    pass

            self.__runner_id = str(data['id'])

        return self.__runner_id

    def _runner_access_level(self):
        data, nexturl = self._gitlab_api_request(
            '/'.join([self._runners_url, self._runner_id]),
        )

        return data['access_level']

    def _set_runner_access_level(self, level):
        self._gitlab_api_request(
            '/'.join([self._runners_url, self._runner_id]),
            'PUT',
            {'access_level': level},
        )

    def _prepare_flags(self):
        flags = []
        for flag in self.flags:
            if not (flag.endswith('=true') or flag.endswith('=false')):
                pattern = '--{}=true'
            else:
                pattern = '--{}'

            flags.append(pattern.format(flag))
        return flags

    def _flatten_key_value_pairs(self, pairs):
        return [part for pair in pairs for part in pair]

    def _repeat_or_pass(self, key, value):
        if not isinstance(value, list):
            return ['--' + key, value]
        return self._flatten_key_value_pairs(
            [['--' + key, inner] for inner in value],
        )

    def _register_runner(self):
        params = self._flatten_key_value_pairs(
            [self._repeat_or_pass(key, value)
             for key, value in self.gitlab_parameters.items()],
        )

        cmd = [self._cmd, 'register', '--non-interactive'] + params
        cmd += self._prepare_flags()
        self.run_command(cmd, check_rc=True)

    def _unregister_runner(self):
        cmd = [
            self._cmd, 'unregister', '--name', self.gitlab_parameters['name'],
        ]
        self.run_command(cmd, check_rc=True)

    @staticmethod
    def documented_params(value):
        """Parse documented parameters into a valid argument_spec.

        To keep the code dry, parse what is already documented in the module's
        `DOCUMENTATION` attribute into a valid python-dictionary. In order to
        let Ansible properly understand suboptions, rename `suboptions` into
        `options`, addtionally skip `description`-keys.
        """
        if not isinstance(value, dict):
            if isinstance(value, list):
                return value
            return value

        new = {}
        for key, value in value.items():
            if key == 'description':
                continue
            if key == 'suboptions':
                key = 'options'
            new[key] = GitLabRunnerModule.documented_params(value)
        return new

    def __init__(self, *args, **kwargs):
        """Create an GitLabRunnerModule-object, in order to manage an instance.

        Prepare the accepted argument_spec, let AnsibleModule check for
        required, invalid and missing arguments (through super.__init__()) and
        provide `gitlab_paramters` and `flags` for advanced readability.
        """
        super(GitLabRunnerModule, self).__init__(
            *args,
            argument_spec=self.documented_params(
                yaml.load(DOCUMENTATION)['options'],
            ),
            supports_check_mode=True,
            required_together=self._required_together,
            **kwargs
        )

        self.__runner_id = None

        # Merge extra parameters into the parent dictionary. This is a
        # workaround for ansible deprecating check_invalid_arguments in 2.9
        # https://github.com/ansible/ansible/pull/34004
        self.gitlab_parameters = self.params['parameters']
        self.gitlab_parameters.update(self.gitlab_parameters.pop('extra'))

        self.flags = self.params['flags']

    def __call__(self):
        """Execute the managing process for a gitlab-runner instance.

        Compare the actual state of an instance and change states, according to
        what is desired (present, protected)
        """
        changed = False

        should_exist = self.params['state'] == 'present'
        desired_access_level = 'ref_protected'

        exists = self.check_mode or self._runner_exists()

        if not self.params.get('protected', False):
            desired_access_level = 'not_protected'

        if exists and not should_exist:
            changed = True
            if not self.check_mode:
                self._unregister_runner()

        elif not exists and should_exist:
            changed = True
            if not self.check_mode:
                self._register_runner()

        if should_exist and self.params.get('personal_access_token', None):
            if self._runner_access_level() != desired_access_level:
                changed = True
                if not self.check_mode:
                    self._set_runner_access_level(desired_access_level)

        result = {}
        result['changed'] = changed
        result['name'] = self.gitlab_parameters['name']
        result['url'] = self.gitlab_parameters['url']
        result['executor'] = self.gitlab_parameters['executor']
        result['state'] = self.params['state']
        self.exit_json(**result)


def main():
    GitLabRunnerModule()()


if __name__ == '__main__':
    main()
